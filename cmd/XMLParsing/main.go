package main

import (
	"io/ioutil"
	"log"

	"github.com/clbanning/mxj"
	"github.com/davecgh/go-spew/spew"
)

func main() {
	soap, err := ioutil.ReadFile("../../../../../../../git/KIT-CA/ca-tools/_api-inspector.response.xml")
	if err != nil {
		log.Fatal(err)
	}
	resp, err := mxj.NewMapXml(soap, false)
	//items, err := resp.ValueForPath("Envelope.Body.getCAInfoResponse.getCAInfoReturn.Roles")
	if err != nil {
		log.Fatal(err)
	}
	/*	imap, ok := items.(map[string]interface{})
		if !ok {
			log.Fatal("unable to convert")
		}
		for k, v := range imap {
			if
		} */
	//data, _ := resp.ValuesForPath("Envelope.Body.getCAInfoResponse.getCAInfoReturn.CAChain")
	data, _ := resp.ValuesForPath("Envelope.Body.getCAInfoResponse.getCAInfoReturn.CAChain.item.#text")
	//data := resp.LeafPaths(true)
	spew.Dump(data)
}
