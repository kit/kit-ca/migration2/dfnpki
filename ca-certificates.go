package dfnpki

import (
	"crypto/x509"
	"errors"
	"time"
)

var (
	DFNCertificatePool *x509.CertPool
	ErrNoTrustChain    = errors.New("Unable to find a trust chain")
)

func init() {
	DFNCertificatePool = x509.NewCertPool()
	for _, c := range DFNCertificateList {
		DFNCertificatePool.AppendCertsFromPEM([]byte(c))
	}
}

// GetChainForCertificate tries to build a chain for a given certificate
func GetChainForCertificate(cert *x509.Certificate) ([]*x509.Certificate, error) {
	// locate system trusted certs, fallback to DFN
	rootPool, err := x509.SystemCertPool()
	if err != nil {
		rootPool = DFNCertificatePool
	}

	// try to find trusted chains
	chains, err := cert.Verify(x509.VerifyOptions{
		Intermediates: DFNCertificatePool,
		Roots:         rootPool,
		CurrentTime:   time.Now(),
	})
	if err != nil {
		return nil, err
	}
	switch len(chains) {
	case 0:
		return nil, ErrNoTrustChain
	case 1:
		return chains[0], nil
	// send first chain if more than one chain applies
	default:
		return chains[0], nil
	}
}
