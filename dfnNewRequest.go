package dfnpki

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"text/template"

	"github.com/clbanning/mxj"
)

type NewRequestData struct {
	RaId                          int
	Pkcs10                        string
	AltNames                      []string
	Role                          DFNRole
	PIN                           string
	AddName, AddEMail, AddOrgUnit string
	Publish                       bool
}

const (
	newRequestTemplate = `<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:ns0="http://schemas.xmlsoap.org/soap/encoding/" xmlns:ns1="https://pki.pca.dfn.de/DFNCERT/Public" xmlns:ns2="http://www.w3.org/2001/XMLSchema" xmlns:ns3="http://schemas.xmlsoap.org/soap/envelope/" SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
<SOAP-ENV:Header/>
<ns3:Body>
<ns1:newRequest>
<RaID xsi:type="ns2:int">{{- .RaId }}</RaID>
<PKCS10 xsi:type="ns2:string">{{- .Pkcs10 }}
</PKCS10>
<AltNames xsi:type="ns1:ArrayOfString" ns0:arrayType="ns0:string[1]">
{{- range .AltNames }}
<item xsi:type="ns0:string">{{- . }}</item>
{{- end }}
</AltNames>
<Role xsi:type="ns2:string">{{- .Role}}</Role>
<Pin xsi:type="ns2:string">{{- .PIN }}</Pin>
<AddName xsi:type="ns2:string">{{- .AddName }}</AddName>
<AddEMail xsi:type="ns2:string">{{- .AddEMail }}</AddEMail>
<AddOrgUnit xsi:type="ns2:string">{{- .AddOrgUnit }}</AddOrgUnit>
<Publish xsi:type="ns2:boolean">{{- if .Publish }}true{{- else }}false{{- end }}</Publish>
</ns1:newRequest>
</ns3:Body>
</SOAP-ENV:Envelope>`
)

var (
	TemplateNewRequest   = template.Must(template.New("newRequest").Parse(newRequestTemplate))
	ErrorNoPEMBlockFound = errors.New("No PEM block found in keybytes")
)

// NewPrivateKey generates a new RSA private key with reasonable key size bounds
func NewPrivateKey(bits int) (*rsa.PrivateKey, error) {
	if bits < 2048 || bits > 8192 {
		bits = 4096
	}
	// generate private key
	pkey, err := rsa.GenerateKey(rand.Reader, bits)
	if err != nil {
		return &rsa.PrivateKey{}, err
	}
	return pkey, nil
}

// ParsePrivateKey parses an RSA private key from a byte array
func ParsePrivateKey(keybytes, password []byte) (*rsa.PrivateKey, error) {
	var (
		err   error
		block *pem.Block
	)
	// try decoding as pkcs1
	rsakey, err := x509.ParsePKCS1PrivateKey(keybytes)
	if err == nil {
		return rsakey, nil
	}
	// try decoding as pkcs8
	p8key, err := x509.ParsePKCS8PrivateKey(keybytes)
	if err == nil {
		if rsakey, ok := p8key.(*rsa.PrivateKey); ok {
			return rsakey, nil
		}
	}
	// try pem encoding
	rest := keybytes
	for {
		// get next block
		block, rest = pem.Decode(rest)
		if block == nil {
			return nil, ErrorNoPEMBlockFound
		}
		// handle encrypted block
		if x509.IsEncryptedPEMBlock(block) {
			decrypted, err := x509.DecryptPEMBlock(block, password)
			if err != nil {
				return nil, err
			} else {
				return ParsePrivateKey(decrypted, password)
			}
		}
		// call self with asn.1 key block, skip block on error
		key, err := ParsePrivateKey(block.Bytes, password)
		if err == nil {
			return key, nil
		}
	}
}

// GenerateRequest creates a PKCS10 (“.req”) request for a given RSA private key
func GenerateRequest(pkey *rsa.PrivateKey, request *x509.CertificateRequest) (string, error) {
	// create pkcs10 certificate request
	req, err := x509.CreateCertificateRequest(rand.Reader, request, pkey)
	if err != nil {
		return "", err
	}
	// return PEM encoded request
	return string(pem.EncodeToMemory(&pem.Block{
		Type:  "NEW CERTIFICATE REQUEST",
		Bytes: req,
	})), nil
}

// NewRequest implements the SOAP API newRequest interface
func NewRequest(client SoapClient, csr string, data NewRequestData) (string, error) {
	var (
		err  error
		resp mxj.Map
	)

	// generate SOAP request
	data.Pkcs10 = csr
	data.PIN = PINtoHash(data.PIN)

	// make SOAP request
	_, resp, err = client.Request(TemplateNewRequest, data)
	if err != nil {
		return "", err
	}

	// look for request id in response
	requestCode, exists := resp.ValueForPathString("Envelope.Body.newRequestResponse.newRequestReturn.#text")
	if exists != nil {
		return "", errors.New("Unable to parse SOAP reply")
	}
	return requestCode, nil
}
