package dfnpki

import (
	"io/ioutil"
	"path/filepath"
	"strings"
	"testing"
)

func TestParsePrivateKey(t *testing.T) {
	files, err := ioutil.ReadDir("tests")
	if err != nil {
		t.Fatal(err)
	}
	for _, file := range files {
		if !file.IsDir() {
			if !strings.HasPrefix(file.Name(), "openssl") {
				continue
			}
			keyfilepath := filepath.Join("tests", file.Name())
			keybytes, err := ioutil.ReadFile(keyfilepath)
			if err != nil {
				t.Fatal(err)
			}
			_, err = ParsePrivateKey(keybytes, []byte("12345678"))
			if err != nil {
				t.Errorf("Error loading keyfile %s: %s", keyfilepath, err)
			}
		}
	}
}
