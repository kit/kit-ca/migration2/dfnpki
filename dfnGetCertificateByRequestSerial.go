package dfnpki

import (
	"errors"
	"github.com/clbanning/mxj"
	"text/template"
)

type GetCertificateByRequestSerialData struct {
	RaId   int
	Serial int
	PIN    string
}

const (
	getCertificateByRequestSerialTemplate = `<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:ns0="https://pki.pca.dfn.de/DFNCERT/Public" xmlns:ns1="http://schemas.xmlsoap.org/soap/encoding/" xmlns:ns2="http://www.w3.org/2001/XMLSchema" xmlns:ns3="http://schemas.xmlsoap.org/soap/envelope/" SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
  <SOAP-ENV:Header/>
  <ns3:Body>
    <ns0:getCertificateByRequestSerial>
      <RaID xsi:type="ns2:int">{{- .RaId }}</RaID>
      <Serial xsi:type="ns2:int">{{- .Serial }}</Serial>
      <Pin xsi:type="ns2:string">{{- .PIN }}</Pin>
    </ns0:getCertificateByRequestSerial>
  </ns3:Body>
</SOAP-ENV:Envelope>`
)

var (
	TemplateGetCertificateByRequestSerial = template.Must(template.New("getCertificateByRequestSerial").Parse(getCertificateByRequestSerialTemplate))
)

// GetCertificateByRequestSerial implements the SOAP API getCertificateByRequestSerial interface
func GetCertificateByRequestSerial(client SoapClient, data GetCertificateByRequestSerialData) (string, error) {
	var (
		err  error
		resp mxj.Map
	)

	// make SOAP request
	_, resp, err = client.Request(TemplateGetCertificateByRequestSerial, data)
	if err != nil {
		return "", err
	}

	// look for certificate
	certificate, exists := resp.ValueForPathString("Envelope.Body.getCertificateByRequestSerialResponse.getCertificateByRequestSerialReturn.#text")
	if exists != nil {
		return "", errors.New("Unable to parse SOAP reply")
	}

	return certificate, nil
}
