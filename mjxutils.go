package dfnpki

import (
	"errors"
	"time"
)

// extractMjxString extracts the value for key as string from an mjx map[string]map[string]interface{}
func extractMjxString(subtree interface{}, key string) (string, error) {
	mjxTree, ok := subtree.(map[string]interface{})
	if !ok {
		return "", errors.New("unable to convert subtree to map")
	}
	rawNode, exists := mjxTree[key]
	if !exists {
		return "", errors.New("unable to find key " + key)
	}
	node, ok := rawNode.(map[string]interface{})
	if !ok {
		return "", errors.New("unable to convert subtree to map")
	}
	// if node does not exist, send empty string
	/*
		_, exists = node["-nil"]
		if exists {
			return "", nil
		}
	*/
	SOAPString, exists := node["#text"]
	if !exists {
		return "", errors.New("unable to find SOAP string")
	}
	nodeSstring, ok := SOAPString.(string)
	if !ok {
		return "", errors.New("unable to convert SOAP value to string")
	}
	return nodeSstring, nil
}

// extractDate extracts value for key as time.Time from an mjx struct
func extractDate(subtree interface{}, key string) (timestamp time.Time, err error) {
	timestring, err := extractMjxString(subtree, key)
	if err != nil {
		return EpochZero, err
	}
	// Mon Jan 2 15:04:05 -0700 MST 2006
	if timestring == "null" {
		return EpochZero, nil
	}

	ts, err := time.Parse(DFNTimestampFormat, timestring)
	if err != nil {
		return time.Now(), err
	}
	return ts, err
}

// extractBool extracts a boolean value for key from an mjx struct
func extractBool(subtree interface{}, key string) (b bool, err error) {
	boolstring, err := extractMjxString(subtree, key)
	if err != nil {
		return false, err
	}
	switch boolstring {
	case "true":
		return true, nil
	case "false":
		return false, nil
	default:
		return false, errors.New("not a boolean")
	}
}
