#!/bin/bash

rm -f -- *.pem
curl http://cdp.pca.dfn.de/global-root-ca/pub/cacert/cacert.pem -o 'DFN-Verein PCA Global - G01.pem'
curl http://cdp.pca.dfn.de/global-root-g2-ca/pub/cacert/cacert.pem -o 'DFN-Verein Certification Authority 2.pem'
curl http://cdp.pca.dfn.de/telekom-root-ca-2/pub/cacert/cacert.pem -o 'Deutsche Telekom Root CA 2.pem'
curl http://pki0336.telesec.de/crt/TeleSec_GlobalRoot_Class_2.cer -o 'T-TeleSec GlobalRoot Class 2.pem'
curl $(echo -n 'http://cdp.pca.dfn.de/{';lynx -listonly -nonumbers -dump https://info.pca.dfn.de/ | fgrep info.pca.dfn.de | cut -d/ -f 4 | tr "\n" "," | sed 's/,$//';echo -n '}/pub/cacert/cacert.pem') -o "#1.pem"
#bzip2 -v -9 *.pem
