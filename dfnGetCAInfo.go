package dfnpki

import (
	"text/template"

	"github.com/clbanning/mxj"
)

type getCAInfoData struct {
	RaId int
}

type CAInfo struct {
	Roles      []string
	DNPrefixes []string
	CAChain    []string
}

const (
	getCAInfoDataTemplate = `<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:ns0="http://schemas.xmlsoap.org/soap/encoding/" xmlns:ns1="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns2="https://pki.pca.dfn.de/DFNCERT/Public" xmlns:ns3="http://www.w3.org/2001/XMLSchema" SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
  <SOAP-ENV:Header/>
  <ns1:Body>
    <ns2:getCAInfo>
      <RaID xsi:type="ns3:int">{{- .RaId }}</RaID>
    </ns2:getCAInfo>
  </ns1:Body>
</SOAP-ENV:Envelope>`
)

var (
	TemplateGetCAInfoData = template.Must(template.New("getCAInfoData").Parse(getCAInfoDataTemplate))
)

// GetCAInfoData implements the getCAInfoData SOAP API interface
func GetCAInfoData(client SoapClient, raid int) (CAInfo, error) {
	var (
		err    error
		resp   mxj.Map
		cainfo CAInfo
	)

	// make SOAP request
	_, resp, err = client.Request(TemplateGetCAInfoData, getCAInfoData{raid})
	if err != nil {
		return CAInfo{}, err
	}

	// get public roles
	rolesList, err := resp.ValuesForPath("Envelope.Body.getCAInfoResponse.getCAInfoReturn.Roles.item.#text")
	if err != nil {
		return CAInfo{}, err
	}
	cainfo.Roles = make([]string, 0, len(rolesList))
	for _, val := range rolesList {
		stringval, ok := val.(string)
		if ok {
			cainfo.Roles = append(cainfo.Roles, stringval)
		}
	}
	// get DNPrefixes
	dnprefixList, err := resp.ValuesForPath("Envelope.Body.getCAInfoResponse.getCAInfoReturn.RAInfos.item.DNPrefixes.item.#text")
	if err != nil {
		return CAInfo{}, err
	}
	cainfo.DNPrefixes = make([]string, 0, len(dnprefixList))
	for _, val := range dnprefixList {
		stringval, ok := val.(string)
		if ok {
			cainfo.DNPrefixes = append(cainfo.DNPrefixes, stringval)
		}
	}
	// get chain
	chainList, err := resp.ValuesForPath("Envelope.Body.getCAInfoResponse.getCAInfoReturn.CAChain.item.#text")
	if err != nil {
		return CAInfo{}, err
	}
	cainfo.CAChain = make([]string, 0, len(chainList))
	for _, val := range chainList {
		stringval, ok := val.(string)
		if ok {
			cainfo.CAChain = append(cainfo.CAChain, stringval)
		}
	}
	return cainfo, nil
}
