package dfnpki

import "testing"

func TestSendChallengeEMail(t *testing.T) {
	client, err := GetAuthHTTPClientFromFiles("RA-Operator-Cert.key", "RA-Operator-Cert.cert", "RA-Operator-Cert.chain")
	if err != nil {
		t.Fatal(err)
	}
	sc := SoapClient{
		Client:  client,
		SoapURL: GenerateDomains("kit-ca-g2"),
	}

	edi, err := ListExtendedDomains(sc, 0)
	if err != nil {
		t.Fatal(err)
	}
	SendChallengeEMail(sc, 0, "kit.wtf", "server", edi.Change)
}
