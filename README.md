This golang library partially implements a frontend for the [DFN PKI SOAP API](https://blog.pki.dfn.de/2019/11/soap-client-version-3-8-1-4-0-2/).

It is used in a few projects:

* [dfnpkitool](https://git.scc.kit.edu/KIT-CA/dfnpkitool)
* [dfn-domain-validation](https://git.scc.kit.edu/KIT-CA/dfn-domain-validation)
* [dfn-domain-validation-check](https://git.scc.kit.edu/KIT-CA/dfn-domain-validation-check)

Please do not run `go test`; there is no mocking layer for the DFN API so all tests are run against the live API.

Dokumentation is sparse, please refer to the tests and the aforementions projects for usage information.
