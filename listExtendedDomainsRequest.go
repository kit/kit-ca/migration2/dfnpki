package dfnpki

import (
	"errors"
	"text/template"
	"time"

	"github.com/clbanning/mxj"
)

const (
	DFNTimestampFormat          = "2006-01-02T15:04:05"
	listExtendedDomainsTemplate = `<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:ns0="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://schemas.xmlsoap.org/soap/encoding/" xmlns:ns2="https://ra.pca.dfn.de/DFNCERT/Domains" xmlns:ns3="http://www.w3.org/2001/XMLSchema" SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
  <SOAP-ENV:Header/>
  <ns0:Body>
    <ns2:listExtendedDomains>
      <RaID xsi:type="ns3:int">{{- .RaId }}</RaID>
    </ns2:listExtendedDomains>
  </ns0:Body>
</SOAP-ENV:Envelope>`
)

type ExtendedDomains struct {
	Change  string
	Domains []DomainInformation
}

type DomainInformation struct {
	Approved              bool
	ApprovedDate          time.Time
	BRVersion             string
	ChallengeMailAddress  string
	LastChallengeMailSent time.Time
	Method                string
	Name                  string
	Secret                bool
	Type                  string
	ValidUntil            time.Time
}

type DomainFilter func(DomainInformation) bool

// Filter returns true if the domainInformation matches the predicate filter
func (domainInformation DomainInformation) Filter(filter DomainFilter) bool {
	return filter(domainInformation)
}

var (
	EpochZero                   = time.Unix(0, 0)
	TemplateListExtendedDomains = template.Must(template.New("listExtendedDomains").Parse(listExtendedDomainsTemplate))
)

// ListExtendedDomains implements the SOAP API listExtendedDomains interface
func ListExtendedDomains(client SoapClient, raid int) (extendedDomains ExtendedDomains, err error) {
	var (
		resp mxj.Map
	)

	// make SOAP request
	_, resp, err = client.Request(TemplateListExtendedDomains, getCAInfoData{raid})
	if err != nil {
		return extendedDomains, err
	}

	change, err := resp.ValueForPath("Envelope.Body.listExtendedDomainsResponse.listExtendedDomainsReturn.Change.#text")
	if err != nil {
		return extendedDomains, err
	}
	// extract change string
	ChangeString, ok := change.(string)
	if !ok {
		return extendedDomains, errors.New("unable to convert Change field into string")
	}
	extendedDomains.Change = ChangeString

	// loop over result domains
	domains, err := resp.ValuesForPath("Envelope.Body.listExtendedDomainsResponse.listExtendedDomainsReturn.Result.item")
	if err != nil {
		return extendedDomains, err
	}

	for _, domain := range domains {
		d, err := extendedDomainsFromMXJ(domain.(map[string]interface{}))
		if err != nil {
			return extendedDomains, err
		}
		extendedDomains.Domains = append(extendedDomains.Domains, d)
	}

	return extendedDomains, nil
}

// extendedDomainsFromMXJ parses a SOAP reply to listExtendedDomains into a DomainInformation struct
func extendedDomainsFromMXJ(subtree map[string]interface{}) (dominfo DomainInformation, err error) {
	// ApprovedDate
	ApprovedDate, err := extractDate(subtree, "ApprovedDate")
	if err == nil {
		dominfo.ApprovedDate = ApprovedDate
	}
	// LastChallengeMailSent
	LastChallengeMailSent, err := extractDate(subtree, "LastChallengeMailSent")
	if err == nil {
		dominfo.LastChallengeMailSent = LastChallengeMailSent
	}
	// ValidUntil
	ValidUntil, err := extractDate(subtree, "ValidUntil")
	if err == nil {
		dominfo.ValidUntil = ValidUntil
	}
	// Method
	Method, err := extractMjxString(subtree, "Method")
	if err == nil {
		dominfo.Method = Method
	}
	// BRVersion
	BRVersion, err := extractMjxString(subtree, "BRVersion")
	if err == nil {
		dominfo.BRVersion = BRVersion
	}
	// ChallengeMailAddress
	ChallengeMailAddress, err := extractMjxString(subtree, "ChallengeMailAddress")
	if err == nil {
		dominfo.ChallengeMailAddress = ChallengeMailAddress
	}
	// Name
	Name, err := extractMjxString(subtree, "Name")
	if err == nil {
		dominfo.Name = Name
	}
	// Type
	Type, err := extractMjxString(subtree, "Type")
	if err == nil {
		dominfo.Type = Type
	}
	// Secret
	Secret, err := extractBool(subtree, "Secret")
	if err == nil {
		dominfo.Secret = Secret
	}
	// Approved
	Approved, err := extractBool(subtree, "Approved")
	if err == nil {
		dominfo.Approved = Approved
	}

	return dominfo, nil
}
