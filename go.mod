module git.scc.kit.edu/KIT-CA/dfnpki

go 1.14

require (
	github.com/clbanning/mxj v1.8.4
	github.com/davecgh/go-spew v1.1.1
	github.com/kr/pretty v0.2.1
	golang.org/x/net v0.0.0-20200707034311-ab3426394381
)
