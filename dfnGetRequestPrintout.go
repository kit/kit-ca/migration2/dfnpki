package dfnpki

import (
	"bytes"
	"errors"
	"text/template"

	"encoding/pem"

	"github.com/clbanning/mxj"
)

type getRequestPrintoutData struct {
	RaId    int
	Serial  string
	PINHash string
}

const (
	getRequestPrintoutTemplate = `<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:ns0="https://pki.pca.dfn.de/DFNCERT/Public" xmlns:ns1="http://www.w3.org/2001/XMLSchema" xmlns:ns2="http://schemas.xmlsoap.org/soap/encoding/" xmlns:ns3="http://schemas.xmlsoap.org/soap/envelope/" SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
  <SOAP-ENV:Header/>
  <ns3:Body>
    <ns0:getRequestPrintout>
      <RaID xsi:type="ns1:int">{{- .RaId }}</RaID>
      <Serial xsi:type="ns1:int">{{- .Serial }}</Serial>
      <Format xsi:type="ns1:string">application/pdf</Format>
      <Pin xsi:type="ns1:string">{{- .PINHash }}</Pin>
    </ns0:getRequestPrintout>
  </ns3:Body>
</SOAP-ENV:Envelope>`
)

var (
	TemplateGetRequestPrintout = template.Must(template.New("getRequestPrintout").Parse(getRequestPrintoutTemplate))
)

// GetRequestPrintout implements the SOAP API getRequestPrintout interface
func GetRequestPrintout(client SoapClient, raid int, serial string, pin string) ([]byte, error) {
	var (
		err      error
		pdfbytes bytes.Buffer
		resp     mxj.Map
	)

	// create request
	requestData := getRequestPrintoutData{
		RaId:    raid,
		Serial:  serial,
		PINHash: PINtoHash(pin),
	}

	// make SOAP request
	_, resp, err = client.Request(TemplateGetRequestPrintout, requestData)
	if err != nil {
		return nil, err
	}

	// look for pdf file
	pdfpem, exists := resp.ValueForPathString("Envelope.Body.getRequestPrintoutResponse.getRequestPrintoutReturn.#text")
	if exists != nil {
		return nil, errors.New("Unable to parse SOAP reply")
	}
	// PEM-decode pdf (encode/pem needs a type)
	pdfbytes.WriteString("-----BEGIN REQUEST PDF-----\n")
	pdfbytes.Write([]byte(pdfpem))
	pdfbytes.WriteString("\n-----END REQUEST PDF-----")
	body, _ := pem.Decode(pdfbytes.Bytes())
	if len(body.Bytes) > 0 {
		return body.Bytes, nil
	}
	return nil, errors.New("Unable to PEM-decode answer")
}
