package dfnpki

import (
	"bytes"
	"errors"
	"fmt"
	"github.com/clbanning/mxj"
	"io/ioutil"
	"net/http"
	"os"
	"text/template"
)

type SoapClient struct {
	Client  *http.Client
	SoapURL string
}

func NewSoapClient(client *http.Client, baseurl string) SoapClient {
	return SoapClient{
		Client:  client,
		SoapURL: baseurl,
	}
}

// generic SOAP call
func (sc *SoapClient) Request(tmpl *template.Template, data interface{}) ([]byte, mxj.Map, error) {
	var err error

	// generate SOAP request
	var requestbody bytes.Buffer
	err = tmpl.Execute(&requestbody, data)
	if err != nil {
		return nil, nil, err
	}
	//fmt.Println(requestbody.String())
	// send soap request
	response, err := sc.Client.Post(sc.SoapURL, "text/xml; charset=utf-8", &requestbody)
	if err != nil {
		return nil, nil, err
	}
	// read response
	responseString, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, nil, err
	}
	response.Body.Close()
	// parse response into map
	resp, err := mxj.NewMapXml(responseString, false)
	if err != nil {
		fmt.Fprintln(os.Stderr, string(responseString))
		return nil, nil, err
	}
	// check if a soap fault occurred
	faultstring, exists := resp.ValueForPathString("Envelope.Body.Fault.faultstring")
	if exists == nil {
		return nil, nil, errors.New(faultstring)
	}
	return responseString, resp, nil
}
