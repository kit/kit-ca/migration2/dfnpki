package dfnpki

import (
	"testing"
	"time"
)

func TestListExtendedDomains(t *testing.T) {
	client, err := GetAuthHTTPClientFromFiles("RA-Operator-Cert.key", "RA-Operator-Cert.cert", "RA-Operator-Cert.chain")
	if err != nil {
		t.Fatal(err)
	}
	sc := SoapClient{
		Client:  client,
		SoapURL: GenerateDomains("kit-ca-g2"),
	}
	extendedDomainInformation, err := ListExtendedDomains(sc, 0)
	if err != nil {
		t.Fatal(err)
	}
	for _, di := range extendedDomainInformation.Domains {
		if di.Filter(func(information DomainInformation) bool {
			return di.Approved &&
				time.Until(di.ValidUntil) < time.Hour*24*14 &&
				time.Since(di.LastChallengeMailSent) > time.Hour*24*7

		}) {
			//pretty.Println(di.Name, di.ValidUntil.String())
		}
		//pretty.Println(di.ChallengeMailAddress)
	}
}
