#!/bin/bash

rm openssl*
numbits=2048
openssl genrsa -out openssl_${numbits}.pem ${numbits}
openssl rsa -in openssl_${numbits}.pem -outform DER -out openssl_${numbits}.der

pass=12345678
for encalg in des des3 aes128 aes256; do
    openssl rsa -in openssl_${numbits}.pem -outform PEM -passout pass:$pass -${encalg} -out openssl_${numbits}_${encalg}.pem
done

for outform in pem der; do
    openssl pkcs8 -in openssl_${numbits}.pem -topk8 -outform $outform -nocrypt -out openssl_${numbits}_${outform}.p8
    #openssl pkcs8 -in openssl_${numbits}.pem -topk8 -outform $outform -passin pass:$pass -passout pass:$pass -out openssl_${numbits}_${outform}_enc.p8
done