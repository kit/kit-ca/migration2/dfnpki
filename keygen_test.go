package dfnpki

import (
	"crypto/rand"
	"crypto/rsa"
	"testing"
)

var (
	pkey rsa.PrivateKey
)

func benchmarkRSAKeygen(size int, b *testing.B) {
	var p *rsa.PrivateKey
	for n := 0; n < b.N; n++ {
		p, _ = rsa.GenerateKey(rand.Reader, size)
	}
	pkey = *p
}

func BenchmarkRSA512(b *testing.B)  { benchmarkRSAKeygen(512, b) }
func BenchmarkRSA1024(b *testing.B) { benchmarkRSAKeygen(1024, b) }
func BenchmarkRSA2048(b *testing.B) { benchmarkRSAKeygen(2048, b) }
func BenchmarkRSA4096(b *testing.B) { benchmarkRSAKeygen(4096, b) }
