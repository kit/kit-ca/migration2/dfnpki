package dfnpki

import (
	"errors"
	"text/template"
	"time"

	"github.com/clbanning/mxj"
)

type SendChallengeEMailData struct {
	RaId   int
	Name   string
	Type   string
	Change string
}

type SendChallengeEMailReturn struct {
	Change                string
	LastChallengeMailSent time.Time
}

const (
	sendChallengeEMailTemplate = `<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:ns0="http://schemas.xmlsoap.org/soap/encoding/" xmlns:ns1="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns2="https://ra.pca.dfn.de/DFNCERT/Domains" xmlns:ns3="http://www.w3.org/2001/XMLSchema" SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
  <SOAP-ENV:Header/>
  <ns1:Body>
    <ns2:sendChallengeEMail>
      <RaID xsi:type="ns3:int">{{- .RaId }}</RaID>
      <Name xsi:type="ns3:string">{{- .Name }}</Name>
      <Type xsi:type="ns3:string">{{- .Type }}</Type>
      <Change xsi:type="ns3:string">{{- .Change }}</Change>
    </ns2:sendChallengeEMail>
  </ns1:Body>
</SOAP-ENV:Envelope>`
)

var (
	TemplateSendChallengeEMailData = template.Must(template.New("sendChallengeEMail").Parse(sendChallengeEMailTemplate))
)

// SendChallengeEMail implements the SOAP API sendChallengeEMail interface
func SendChallengeEMail(client SoapClient, raid int, name string, Type string, change string) (response SendChallengeEMailReturn, err error) {
	var (
		resp mxj.Map
	)

	response.LastChallengeMailSent = EpochZero

	// make SOAP request
	_, resp, err = client.Request(TemplateSendChallengeEMailData, SendChallengeEMailData{
		RaId:   raid,
		Name:   name,
		Type:   Type,
		Change: change,
	})
	if err != nil {
		return response, err
	}

	Change, err := resp.ValueForPath("Envelope.Body.sendChallengeEMailResponse.sendChallengeEMailReturn.Change.#text")
	if err != nil {
		return response, err
	}
	ChangeString, ok := Change.(string)
	if !ok {
		return response, errors.New("unable to convert Change SOAP value to string")
	}
	response.Change = ChangeString

	LastChallengeEMailSent, err := resp.ValueForPath("Envelope.Body.sendChallengeEMailResponse.sendChallengeEMailReturn.LastChallengeEMailSent.#text")
	if err != nil {
		return response, err
	}
	LastChallengeEMailSentString, ok := LastChallengeEMailSent.(string)
	if !ok {
		LastChallengeEMailSentString = "1970-01-01T00:00:00"
	}
	ts, err := time.Parse(DFNTimestampFormat, LastChallengeEMailSentString)
	if err != nil {
		return response, err
	}
	response.LastChallengeMailSent = ts

	return response, nil
}
