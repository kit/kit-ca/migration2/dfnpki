package dfnpki

import (
	"crypto/sha1"
	"crypto/tls"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"net/http/cookiejar"
	"time"

	//	ctb "git.scc.kit.edu/heiko.reese/CertificateToolbox"
	"golang.org/x/net/publicsuffix"
)

const (
	pinLength                = 16
	defaultHTTPClientTimeout = 30
)

var (
	pinCharset    = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
	seededRand    = rand.New(rand.NewSource(time.Now().UnixNano()))
	// RolesMachines are all the possible roles machine certificates can have
	// as defined in  https://www.ca.kit.edu/p/zertifikatsprofile
	RolesMachines = []string{
		"802.1X Client",
		"Domain Controller",
		"Exchange Server",
		"LDAP Server",
		"Mail Server",
		"Radius Server",
		"Shibboleth IdP SP",
		"VoIP Server",
		"VPN Server",
		"Web Server",
		"Webserver MustStaple",
		"Web Server SOAP",
	}
	// RolesUser are all the possible roles personal certificates can have
	// as defined in  https://www.ca.kit.edu/p/zertifikatsprofile
	RolesUser = []string{
		"802.1X User",
		"Code Signing",
		"Mitarbeiter",
		"RA Operator",
		"Smartcard",
		"Smartcard Encrypt",
		"Smartcard Logon",
		"Smartcard Sign",
		"Smartcard Sign andLogon",
		"Student",
		"TrustedDisk",
		"User",
		"UserAuth",
		"UserEMail",
		"UserEncrypt",
		"UserSign",
		"UserSignAuth",
		"User SOAP",
		"VPN User",
	}
)

// GetAuthHTTPClient returns an http.client with certificate authentication (using files)
func GetAuthHTTPClientFromFiles(keyfile, certfile, chainfile string) (*http.Client, error) {
	var (
		err error
	)
	// load key and certificate
	keyBytes, err := ioutil.ReadFile(keyfile)
	if err != nil {
		return &http.Client{}, err
	}
	certBytes, err := ioutil.ReadFile(certfile)
	if err != nil {
		return &http.Client{}, err
	}
	chainBytes, err := ioutil.ReadFile(chainfile)
	if err != nil {
		return &http.Client{}, err
	}

	return GetAuthHTTPClient(keyBytes, certBytes, chainBytes)
}

// GetAuthHTTPClient returns an http.client with certificate authentication
func GetAuthHTTPClient(keyBytes, certBytes, chainBytes []byte) (*http.Client, error) {
	var (
		err error
	)
	// create in-memory cookie jar
	jar, err := cookiejar.New(&cookiejar.Options{PublicSuffixList: publicsuffix.List})
	if err != nil {
		return &http.Client{}, err
	}

	keycert, err := tls.X509KeyPair(append(certBytes[:], chainBytes[:]...), keyBytes)
	if err != nil {
		return &http.Client{}, err
	}

	// Setup HTTPS client
	tlsConfig := &tls.Config{
		Certificates: []tls.Certificate{keycert},
		MinVersion:   tls.VersionTLS12,
	}
	return &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: tlsConfig,
		},
		Timeout: time.Second * defaultHTTPClientTimeout,
		Jar:     jar,
	}, nil
}

// GetPublicHTTPClient returns a simple http client
func GetPublicHTTPClient() (*http.Client, error) {
	// create in-memory cookie jar
	jar, err := cookiejar.New(&cookiejar.Options{PublicSuffixList: publicsuffix.List})
	if err != nil {
		return &http.Client{}, err
	}
	return &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				MinVersion: tls.VersionTLS12,
			},
		},
		Timeout: time.Second * defaultHTTPClientTimeout,
		Jar:     jar,
	}, nil
}

const (
	endpointTemplatePublic       = "https://pki.pca.dfn.de/%s/cgi-bin/pub/soap/DFNCERT/Public"
	endpointTemplateRegistration = "https://ra.pca.dfn.de/%s/cgi-bin/ra/soap/DFNCERT/Registration"
	endpointTemplateDomains      = "https://ra.pca.dfn.de/%s/cgi-bin/ra/soap/DFNCERT/Domains"
)

type DFNRole string

// endpointURLFactory takes an URL template and returns a function
// that takes a CA name and returns the correct SOAP API endpoint
func endpointURLFactory(templ string) func(string) string {
	return func(raname string) string {
		return fmt.Sprintf(templ, raname)
	}
}

const (
	SANEmail = iota
	SANDNS
	IP
	URI
	MicrosoftGUID
	MicrosoftUPN
)

// SAN returns valid AltNames strings
func SAN(kind int, value string) string {
	switch kind {
	case SANEmail:
		return "email:" + value
	case SANDNS:
		return "DNS:" + value
	case IP:
		return "IP:" + value
	case URI:
		return "URI:" + value
	case MicrosoftGUID:
		return "Microsoft_GUID:" + value
	case MicrosoftUPN:
		return "Microsoft_UPN:" + value
	default:
		return value
	}
}

var (
	GeneratePublicURL    = endpointURLFactory(endpointTemplatePublic)
	GenerateRegistration = endpointURLFactory(endpointTemplateRegistration)
	GenerateDomains      = endpointURLFactory(endpointTemplateDomains)
	// TODO: add hidden roles
	DFNPublicRoles = make(map[string]DFNRole)
)

func init() {
	// source: https://www.pki.dfn.de/fileadmin/PKI/anleitungen/DFN-PKI-Zertifikatprofile_Global.pdf
	for _, role := range append(RolesMachines, RolesUser...) {
		DFNPublicRoles[role] = DFNRole(role)
	}
}

// PINtoHash returns the correct hash for cleartext PINs
func PINtoHash(pin string) string {
	sha1sum := sha1.Sum([]byte(pin))
	return hex.EncodeToString(sha1sum[:])
}

// RandomPIN generates a random PIN (_not_ cryptographically sound)
func RandomPIN() string {
	pin := make([]rune, pinLength)
	for idx := range pin {
		idx2 := seededRand.Intn(len(pinCharset))
		pin[idx] = pinCharset[idx2]
	}
	return string(pin)
}
